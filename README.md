# Simulation

### Model

Sample space: 10000

Idea is the have a list of hash-maps that we could make use of in order to do different calculations.

The primary structure of a single customer should be like:

```clojure
{
  :arrival-time  some-val
  :service-start some-val
  :service-time  some-val
  :service-end   some-val
  :wait-time     some-val
}
```
The values can be generated using the given equations and pre-defined values.

### Usage

1. To get the data related to different kind of cusomters,
```clojure
   simulation.core> (yellow-customer-simulation 10000)
   AVERAGE YELLOW CUSTOMER WAITING TIME:  33330.77268487499
   MAXIMUM WAITING TIME:  66417.9992080687
   DIFFERENCE:  33087.226523193705
   MAXIMUM QUEUE LENGTH 9999
   nil
   simulation.core> (red-customer-simulation 10000)
   AVERAGE YELLOW CUSTOMER WAITING TIME:  168049.89476277735
   MAXIMUM WAITING TIME:  335326.7348596767
   DIFFERENCE:  167276.84009689934
   MAXIMUM QUEUE LENGTH 9999
   nil
   simulation.core> (blue-customer-simulation 10000)
   AVERAGE YELLOW CUSTOMER WAITING TIME:  198450.91271431473
   MAXIMUM WAITING TIME:  394586.388623712
   DIFFERENCE:  196135.47590939727
   MAXIMUM QUEUE LENGTH 9999
   nil
```

From the above we can answer some of the questions.
1. Given only yellow customers, what are the average and maximum customer waiting times?
    - AVERAGE WAITING TIME:  33330.77268487499
    - MAXIMUM WAITING TIME:  66417.9992080687

2. Given only red customers, what are the average and maximum queue lengths in-front of the teller?
    - MAXIMUM QUEUE LENGTH: 9999
    - AVERAGE QUEUE LENGTH: 9999
    
3. Which type of customer(yellow, red or blue) gives the gives the closest value between the average and maximum customer waiting times?
    - DIFFERENCE YELLOW: 33087.226523193705
    - DIFFERENCE RED:  167276.84009689934
    - DIFFERENCE BLUE:  196135.47590939727
  Therefore, the order of difference is blue > red > yellow. Yellow having the average waiting time closest to it's maximum waiting time for a customer.

#### OR 

Just run `(-main)` in order to get the results of all the simulation with a sample space of 10,000:

```clojure
simulation.core> (-main)
AVERAGE YELLOW CUSTOMER WAITING TIME:  33716.32258874248
MAXIMUM WAITING TIME:  67029.66324923163
DIFFERENCE OF MAXIMUM AND AVERAGE:  33313.34066048915
MAXIMUM QUEUE LENGTH 9999

AVERAGE BLUE CUSTOMER WAITING TIME:  204673.89121931582
MAXIMUM WAITING TIME:  403318.0040489727
DIFFERENCE OF MAXIMUM AND AVERAGE:  198644.11282965686
MAXIMUM QUEUE LENGTH 9999

AVERAGE BLUE CUSTOMER WAITING TIME:  167360.0555174667
MAXIMUM WAITING TIME:  334223.7098101917
DIFFERENCE OF MAXIMUM AND AVERAGE:  166863.654292725
MAXIMUM QUEUE LENGTH 9999
nil
```

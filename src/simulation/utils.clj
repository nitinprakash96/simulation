(ns simulation.utils
  (:gen-class))


(defn average
  "Calculate the average of a collection

  Eg: (avergae [1 2 3 4 5])
      => 3"
  [coll]
  (/ (reduce + coll) (count coll)))


(defn split-by
  "Instead of splitting only the first time pred returns false, it
  splits (lazily) every time it turns from true to false.

  Eg: (split-by #(zero? (mod % 2)) [1 2 2 2 3 4 4 5 6 7])
      => ((1) (2 2 2 3) (4 4 5) (6 7))"
  [pred coll]
  (lazy-seq
   (when-let [s (seq coll)]
     (let [!pred (complement pred)
           [xs ys] (split-with !pred s)]
       (if (seq xs)
         (cons xs (split-by pred ys))
         (let [skip (take-while pred s)
               others (drop-while pred s)
               [xs ys] (split-with !pred others)]
           (cons (concat skip xs)
                 (split-by pred ys))))))))

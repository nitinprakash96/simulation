(ns simulation.core
  (:require [simulation.utils :refer [average split-by]])
  (:gen-class))

;; Declare predefined values
(defonce rho 200)

(defonce lambda 100)

(defonce customer-params {:yellow {:a 2 :b 5}
                          :red    {:a 2 :b 2}
                          :blue   {:a 5 :b 1}})

(defonce customer-count 10000)


(defn service-time
  "Get the service time of a customer. The return value depends on the
  type of customer."
  [alpha beta]
  (let [x (rand)]
    (* rho (Math/pow x (- alpha 1)) (Math/pow (- 1 x) (- beta 1)))))


(defn arrival-probablity
  "Probablity that a customer arrives at t-th second.
  This is also a Cummulative Distribution Function (CDF)."
  [t]
  (* (/ 1 lambda) (Math/pow (Math/E) (/ (- t) lambda))))


(defn arrival-value
  "This is the inverse of the CDF for arrival-probablity. This is ideal
  in order to calculate a value from that could correspond to a given
  probablity."
  [t]
  (* (- 100) (Math/log10 (- 1 t))))


(defn sample-customers
  "Generate a list of customer data (only has arrival-time of a customer)

  Eg: (sample-customers 2)
      => ({:arrival-time 15.16468877663772}
          {:arrival-time 44.40775803973056})"
  [n]
  (->> (for [_ (range n)]
         (assoc {} :arrival-time (arrival-value (rand))))
       (sort-by :arrival-time)))


(defn customer-service-times
  "Calculate the service times for each customer. The service timings
  are based on the type of customer (yellow, red or blue)

  Eg: (customer-service-times 2 'yellow')
      => ({:arrival-time 13.575122978988302,
           :service-time 1.8459919006725025}
          {:arrival-time 63.42388693153096,
           :service-time 0.8932495740263595})"
  [n customer-type]
  (let [{:keys [a b]} ((keyword customer-type) customer-params)
        arrival-stats (sample-customers n)]
    (for [customer arrival-stats]
      (assoc customer :service-time (service-time a b)))))



(defn generate-queue-with-customer-stats
  "Generates a full list of customer with data such as service-start,
  service-end and wait-time timings.

  Eg: (generate-queue-with-customer-stats 2 'yellow')
      => [{:arrival-time 25.023718647346293,
           :service-time 0.25570860553637653,
           :service-start 25.023718647346293,
           :service-end 25.27942725288267,
           :wait-time 0.0}
          {:arrival-time 63.90000934034721,
           :service-time 0.007724363425690629,
           :service-start 63.90000934034721,
           :service-end 63.9077337037729,
           :wait-time 0.0}]"
  [n customer-type]
  (let [customers (customer-service-times n customer-type)]
    (loop [customer customers
           last-customer []
           res []]
      (if customer
        (let [c (first customer)
              service-start (max (:arrival-time c)
                                 (or (:service-end last-customer) 0))
              new-customer (conj res (assoc c
                                            :service-start service-start
                                            :service-end (+ service-start
                                                            (:service-time c))
                                            :wait-time (- service-start
                                                          (:arrival-time c))))]
          (recur (next customer)
                 (last new-customer)
                 new-customer))
        res))))


(defn yellow-customer-simulation
  "Run the simulation for yellow customers."
  [n]
  (let [customers (generate-queue-with-customer-stats n "yellow")
        waiting-times (map :wait-time customers)
        queue-lengths (split-by #(> % 0) waiting-times)]
    (println "AVERAGE YELLOW CUSTOMER WAITING TIME: " (average waiting-times))
    (println "MAXIMUM WAITING TIME: " (apply max waiting-times))
    (println "DIFFERENCE OF MAXIMUM AND AVERAGE: "
             (- (apply max waiting-times) (average waiting-times)))
    (println "MAXIMUM QUEUE LENGTH" (->> (for [i queue-lengths] (count i))
                                         (apply max)))))


(defn blue-customer-simulation
  "Run the simulation for blue customers."
  [n]
  (let [customers (generate-queue-with-customer-stats n "blue")
        waiting-times (map :wait-time customers)
        queue-lengths (split-by #(> % 0) waiting-times)]
    (println "AVERAGE BLUE CUSTOMER WAITING TIME: " (average waiting-times))
    (println "MAXIMUM WAITING TIME: " (apply max waiting-times))
    (println "DIFFERENCE OF MAXIMUM AND AVERAGE: "
             (- (apply max waiting-times) (average waiting-times)))
    (println "MAXIMUM QUEUE LENGTH" (->> (for [i queue-lengths] (count i))
                                         (apply max)))))


(defn red-customer-simulation
  "Run the simulation for red customers."
  [n]
  (let [customers (generate-queue-with-customer-stats n "red")
        waiting-times (map :wait-time customers)
        queue-lengths (split-by #(> % 0) waiting-times)]
    (println "AVERAGE BLUE CUSTOMER WAITING TIME: " (average waiting-times))
    (println "MAXIMUM WAITING TIME: " (apply max waiting-times))
    (println "DIFFERENCE OF MAXIMUM AND AVERAGE: "
             (- (apply max waiting-times) (average waiting-times)))
    (println "MAXIMUM QUEUE LENGTH" (->> (for [i queue-lengths] (count i))
                                         (apply max)))))


(defn -main
  "I don't do a whole lot ... yet."
  [& _]
  (yellow-customer-simulation customer-count)
  (newline)
  (blue-customer-simulation customer-count)
  (newline)
  (red-customer-simulation customer-count))
